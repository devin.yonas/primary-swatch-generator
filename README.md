## Primary Swatch Color Generator


#### Screenshot:
![example image](assets/sc1.png "example")

#### Reference:
- [Hex String to int](https://www.thiscodeworks.com/hex-string-to-color-in-flutter-dart-flutter/608230368ca1780014213373)
- [Primary Swatch Generator](https://medium.com/@morgenroth/using-flutters-primary-swatch-with-a-custom-materialcolor-c5e0f18b95b0)
