import 'package:flutter/material.dart';
import 'package:primary_swatch_maker/color_generator.dart';
import 'package:flutter/services.dart';

class PrimarySwatchMakerApp extends StatefulWidget {
  const PrimarySwatchMakerApp({Key? key}) : super(key: key);

  @override
  State<PrimarySwatchMakerApp> createState() => _PrimarySwatchMakerAppState();
}

class _PrimarySwatchMakerAppState extends State<PrimarySwatchMakerApp> {
  MaterialColor? _colorSwatch;
  String? inputColorHex;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Primary Swatch Maker'),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: TextField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Input Primary Color Hex',
                        ),
                        onChanged: (text) {
                          setState(() {
                            inputColorHex = text;
                          });
                        },
                      ),
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        final hex =
                            AppColors.getColorHexFromStr(inputColorHex ?? '');
                        _colorSwatch =
                            AppColors.generateMaterialColor(Color(hex));
                        setState(() {});
                      },
                      child: const SizedBox(
                          height: 50,
                          child: Center(child: Text('Generate Color')))),
                ],
              ),
              Flexible(
                child: _colorSwatch == null
                    ? const SizedBox.shrink()
                    : ListView(
                        children: [
                          _ColorHexContainer(
                              title: 'shade50',
                              color: _colorSwatch?.shade50 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade100',
                              color: _colorSwatch?.shade100 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade200',
                              color: _colorSwatch?.shade200 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade300',
                              color: _colorSwatch?.shade300 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade400',
                              color: _colorSwatch?.shade400 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade500',
                              color: _colorSwatch?.shade500 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade600',
                              color: _colorSwatch?.shade600 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade700',
                              color: _colorSwatch?.shade700 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade800',
                              color: _colorSwatch?.shade800 ?? Colors.red),
                          _ColorHexContainer(
                              title: 'shade900',
                              color: _colorSwatch?.shade900 ?? Colors.red),
                        ],
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _ColorHexContainer extends StatelessWidget {
  final String title;
  final Color color;
  const _ColorHexContainer({required this.title, required this.color, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final text = color.value.toRadixString(16);
    return GestureDetector(
      onTap: () {
        _copyToClipboard(context, text);
      },
      child: Container(
        padding: const EdgeInsets.all(12),
        height: 50,
        color: color,
        child: Row(
          children: [
            Text(
              title,
            ),
            Flexible(
              child: Center(
                child: Text(
                  text,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            const Icon(Icons.copy_rounded),
          ],
        ),
      ),
    );
  }

  Future<void> _copyToClipboard(BuildContext context, String text) async {
    await Clipboard.setData(ClipboardData(text: text));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('$text Copied to clipboard'),
    ));
  }
}
