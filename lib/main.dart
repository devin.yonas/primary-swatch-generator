import 'package:flutter/material.dart';
import 'package:primary_swatch_maker/primary_swatch_maker_app.dart';

void main() {
  runApp(const PrimarySwatchMakerApp());
}
